//
//  FlickrManager.swift
//  PicMe
//
//  Created by William Wyatt on 9/7/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import Foundation
import CoreLocation
import OAuthSwift

public class FlickrManager {
    
    static let sharedManager = FlickrManager()
    
    let consumerKey = Flickr["consumerKey"]!
    let consumerSecret = Flickr["consumerSecret"]!
    var oauthswift:OAuth1Swift?
    
    func setOAuthCreds(oauthToken: String, oauthTokenSecret: String, serviceParameters: [String:String]) {
        self.oauthswift = OAuth1Swift(
            consumerKey     : serviceParameters["consumerKey"]!,
            consumerSecret  : serviceParameters["consumerSecret"]!,
            requestTokenUrl : "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl    : "https://www.flickr.com/services/oauth/authorize?perms=write",
            accessTokenUrl  : "https://www.flickr.com/services/oauth/access_token"
        )
//        self.oauthswift?.setCredentials(oauthToken, oauthTokenSecret: oauthTokenSecret)
    }
    
    // do oauth for flickr
    func doOAuthFlickr(serviceParameters: [String:String], completion: (() -> Void)?){
        let oauthswift = OAuth1Swift(
            consumerKey     : serviceParameters["consumerKey"]!,
            consumerSecret  : serviceParameters["consumerSecret"]!,
            requestTokenUrl : "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl    : "https://www.flickr.com/services/oauth/authorize?perms=write",
            accessTokenUrl  : "https://www.flickr.com/services/oauth/access_token"
        )
        
        oauthswift.authorizeWithCallbackURL(
            NSURL(string: "picme://oauth-callback/flickr")!,
            success: {
                credential, response, parameters in
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(credential.oauth_token, forKey: "oauthToken")
                defaults.setObject(credential.oauth_token_secret, forKey: "oauthTokenSecret")
                self.setOAuthCreds(credential.oauth_token, oauthTokenSecret: credential.oauth_token_secret, serviceParameters: serviceParameters)
                completion?()
            }, failure: { error in
                print(error.localizedDescription)
        })
    }
    
    func getLocalPhotos (location: CLLocation?, completion: ((imageURLs: [String]) -> Void)?) {
        guard let oauthswift = self.oauthswift else { return }
        let url :String = "https://api.flickr.com/services/rest/"
        let parameters :Dictionary = [
            "method"             : "flickr.photos.getRecent",  // method needs to be changed to flickr.photos.geo.photosForLocation
            "oauth_consumer_key" : consumerKey,
            "lat"                : "\(location?.coordinate.latitude)",
            "lon"                : "\(location?.coordinate.longitude)",
            "accuracy"           : "12",
            "format"             : "json",
            "per_page"           : "10",
            "nojsoncallback"     : "1"
        ]
        
        oauthswift.client.get(url, parameters: parameters,
            success: {
                data, response in
                let jsonDict: AnyObject! = try? NSJSONSerialization.JSONObjectWithData(data, options: [])
                completion?(imageURLs: self.convertJSONToImageURL(jsonDict))
            }, failure: { error in
                print(error)
                self.doOAuthFlickr(Flickr, completion: nil)
        })
    }
    
    func convertJSONToImageURL(jsonDict: AnyObject) -> [String] {
        
        guard let results = jsonDict["photos"] as? [String: AnyObject] else { return [] }
        guard let photosArray = results["photo"] as? [AnyObject] else { return [] }
        
        var imageURLs:[String] = []
        
        for photo in photosArray {
            
            if let photoInfo:[String: AnyObject] = photo as? [String: AnyObject],
               let farm:Int = photoInfo["farm"] as? Int,
               let server:String = photoInfo["server"] as? String,
               let photoID:String = photoInfo["id"] as? String,
               let secret:String = photoInfo["secret"] as? String {
                        let url = "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret).jpg"
                        imageURLs.append(url)
            } else {
                print("Invalid URL")
            }
        }
        return imageURLs
    }

}

