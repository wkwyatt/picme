//
//  CardViewBackground.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//


import Foundation
import UIKit

class CardViewBackground: UIView, CardViewDelegate {
    var exampleCardLabels: [String]?
    var picMeImages: [String]!
    var allCards: [CardView]!
    
    let MAX_BUFFER_SIZE = 2
    let CARD_HEIGHT: CGFloat = 386
    let CARD_WIDTH: CGFloat = 290
    
    var cardsLoadedIndex: Int!
    var loadedCards: [CardView]!
    var menuButton: UIButton!
    var messageButton: UIButton!
    var checkButton: UIButton!
    var xButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, picURLs:[String]) {
        super.init(frame: frame)
        super.layoutSubviews()
        self.setupView()
        picMeImages = picURLs
        allCards = []
        loadedCards = []
        cardsLoadedIndex = 0
        self.loadCards()
    }
    
    func setupView() -> Void {
        self.backgroundColor = UIColor(red: 0.92, green: 0.93, blue: 0.95, alpha: 1)
        
        xButton = UIButton(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2 + 35, self.frame.size.height/2 + CARD_HEIGHT/2 + 10, 59, 59))
        xButton.setImage(UIImage(named: "xButton"), forState: UIControlState.Normal)
        xButton.addTarget(self, action: #selector(CardViewBackground.swipeLeft), forControlEvents: UIControlEvents.TouchUpInside)
        
        checkButton = UIButton(frame: CGRectMake(self.frame.size.width/2 + CARD_WIDTH/2 - 85, self.frame.size.height/2 + CARD_HEIGHT/2 + 10, 59, 59))
        checkButton.setImage(UIImage(named: "checkButton"), forState: UIControlState.Normal)
        checkButton.addTarget(self, action: #selector(CardViewBackground.swipeRight), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.addSubview(xButton)
        self.addSubview(checkButton)
    }
    
    func createCardViewWithDataAtIndex(index: Int) -> CardView {
        let cardView = CardView(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT)/2, CARD_WIDTH, CARD_HEIGHT))
        if let imageURL:NSURL = NSURL(string: self.picMeImages[index]) {
            cardView.flickrPic?.loadImageUsingCacheWithURLString(imageURL)
        }
        cardView.delegate = self
        return cardView
    }
    
    func loadCards() -> Void {
        if picMeImages.count > 0 {
            let numLoadedCardsCap = picMeImages.count > MAX_BUFFER_SIZE ? MAX_BUFFER_SIZE : picMeImages.count
            for i in 0 ..< picMeImages.count {
                let newCard: CardView = self.createCardViewWithDataAtIndex(i)
                newCard.flickrImageURL = self.picMeImages[i]
                allCards.append(newCard)
                if i < numLoadedCardsCap {
                    loadedCards.append(newCard)
                }
            }
            
            for i in 0 ..< loadedCards.count {
                if i > 0 {
                    self.insertSubview(loadedCards[i], belowSubview: loadedCards[i - 1])
                } else {
                    self.addSubview(loadedCards[i])
                }
                cardsLoadedIndex = cardsLoadedIndex + 1
            }
        }
    }
    
    func updatePicsSelected(imageURL: String?, list: String) {
        guard let url = imageURL else {return}
        let defaults = NSUserDefaults.standardUserDefaults()
        
        var storedPics = defaults.objectForKey(list) as? [String] ?? [String]()
        storedPics.append(url)
        defaults.setObject(storedPics, forKey: list)
        
        defaults.synchronize()
    }
    
    func cardSwipedLeft(card: UIView) -> Void {
        self.updatePicsSelected(loadedCards.first?.flickrImageURL, list: "ditched")
        loadedCards.removeAtIndex(0)
        
        if cardsLoadedIndex < allCards.count {
            loadedCards.append(allCards[cardsLoadedIndex])
            cardsLoadedIndex = cardsLoadedIndex + 1
            self.insertSubview(loadedCards[MAX_BUFFER_SIZE - 1], belowSubview: loadedCards[MAX_BUFFER_SIZE - 2])
        }
    }
    
    func cardSwipedRight(card: UIView) -> Void {
        self.updatePicsSelected(loadedCards.first?.flickrImageURL, list: "picked")
        loadedCards.removeAtIndex(0)
        
        if cardsLoadedIndex < allCards.count {
            loadedCards.append(allCards[cardsLoadedIndex])
            cardsLoadedIndex = cardsLoadedIndex + 1
            self.insertSubview(loadedCards[MAX_BUFFER_SIZE - 1], belowSubview: loadedCards[MAX_BUFFER_SIZE - 2])
        }
    }
    
    func swipeRight() -> Void {
        if loadedCards.count <= 0 {
            return
        }
        let dragView: CardView = loadedCards[0]
        dragView.overlayView.setMode(OverlayViewMode.OverlayViewModeRight)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.rightClickAction()
    }
    
    func swipeLeft() -> Void {
        if loadedCards.count <= 0 {
            return
        }
        let dragView: CardView = loadedCards[0]
        dragView.overlayView.setMode(OverlayViewMode.OverlayViewModeLeft)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.leftClickAction()
    }
}
