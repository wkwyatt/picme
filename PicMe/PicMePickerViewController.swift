//
//  PicMePickerViewController.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import UIKit
import CoreLocation

class PicMePickerViewController: UIViewController, CLLocationManagerDelegate {
    
    
    let locationManager = CLLocationManager()
    var location:CLLocation?
    
    let activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0,150,150)) as UIActivityIndicatorView

    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.loadPicker()
        // Do any additional setup after loading the view.
    }
    
    func loadPicker() {
        FlickrManager.sharedManager.getLocalPhotos(self.location) {
            imageURLs in
            let cardBackground: CardViewBackground = CardViewBackground(frame: self.view.frame, picURLs: imageURLs)
            self.view.addSubview(cardBackground)
        }
    }

    @IBAction func refreshButtonTouched(sender: AnyObject) {
        self.activityIndicator.startAnimating()
        FlickrManager.sharedManager.getLocalPhotos(self.location) {
            imageURLs in
            self.activityIndicator.stopAnimating()
            let cardBackground: CardViewBackground = CardViewBackground(frame: self.view.frame, picURLs: imageURLs)
            self.view.addSubview(cardBackground)
        }
    }
}

/**********     CoreLocation Delegate Methods      ***********/

extension PicMePickerViewController {
    
    // CoreLocation Delegate Methods
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
        print("THIS IS THE LOCATION: \(location)")
        
        // Center map based on location
        //        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        
        //        self.mapView.setRegion(region, animated; true)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Errors: \(error.localizedDescription)")
    }
}