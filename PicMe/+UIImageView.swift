//
//  +UIImageView.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func loadImageUsingCacheWithURLString(url: NSURL) {
        let imageCache = NSCache()
        self.image = nil
    
        // First check if there is an image in the cache
        if let cachedImage = imageCache.objectForKey(url) as? UIImage {
            self.image = cachedImage
            return
        } else {
            // Otherwise download image using the url
            NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, response, error) in
                if error != nil {
                    print(error)
                } else {
                    dispatch_async(dispatch_get_main_queue(), {
                        // Cache to image so it doesn't need to be reloaded every time the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            imageCache.setObject(downloadedImage, forKey: url)
                            self.image = downloadedImage
                        }
                    })
                }
            }).resume()
        }
    }
}