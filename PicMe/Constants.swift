//
//  Constants.swift
//  PicMe
//
//  Created by William Wyatt on 9/8/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import Foundation

let Flickr =
[
    "api_key"       : "f077e06d7cac50b63929d40069b9dfc7",
    "api_secret"    : "a1ff2aae17371f67",
    "consumerKey"   : "f077e06d7cac50b63929d40069b9dfc7",
    "consumerSecret": "a1ff2aae17371f67",
    "base_url"      : "https://api.flickr.com/services/",
    "callback_url"  : "picme://oauth-callback/flickr"
]

let flickrImageURLs =
[
    "https://farm9.staticflickr.com/8387/29569681015_o-9b85563c6d_o.png",
    "https://farm9.staticflickr.com/8387/29569681015_o-9b85563c6d_o.png",
    "https://farm9.staticflickr.com/8472/28942650563_o-e911280c93_o.png",
    "https://farm9.staticflickr.com/8472/28942650563_o-e911280c93_o.png",
    "https://farm9.staticflickr.com/8387/29569681015_o-9b85563c6d_o.png",
    "https://farm9.staticflickr.com/8472/28942650563_o-e911280c93_o.png"
]