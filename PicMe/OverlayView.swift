//
//  OverlayView.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//


import Foundation
import UIKit

enum OverlayViewMode {
    case OverlayViewModeLeft
    case OverlayViewModeRight
}

class OverlayView: UIView{
    var _mode: OverlayViewMode! = OverlayViewMode.OverlayViewModeLeft
    var imageView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        imageView = UIImageView(image: UIImage(named: "noButton"))
        self.addSubview(imageView)
    }
    
    func setMode(mode: OverlayViewMode) -> Void {
        if _mode == mode {
            return
        }
        _mode = mode
        
        if _mode == OverlayViewMode.OverlayViewModeLeft {
            imageView.image = UIImage(named: "noButton")
        } else {
            imageView.image = UIImage(named: "yesButton")
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = CGRectMake(50, 50, 100, 100)
    }
}