//
//  PicMeHomeVC.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import UIKit
import Alamofire
import OAuthSwift
import MapKit
import CoreLocation

class PicMeHomeVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var flickrLoginButton: UIButton!

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let filePath = NSBundle.mainBundle().pathForResource("camera_background", ofType: "gif")
        let gif = NSData(contentsOfFile: filePath!)
        
        webView.loadData(gif!, MIMEType: "image/gif", textEncodingName: String(), baseURL: NSURL())
        
//        let filter = UIView()
//        filter.frame = self.view.frame
//        filter.backgroundColor = UIColor.blackColor()
//        filter.alpha = 0.05
//        self.view.addSubview(filter)
        
        self.flickrLoginButton.contentEdgeInsets = UIEdgeInsetsMake(15.0, 30.0, 15.0, 30.0);
    }

    @IBAction func FlickrAuthButtonTouched(sender: AnyObject) {
        FlickrManager.sharedManager.doOAuthFlickr(Flickr) {
            self.performSegueWithIdentifier("showPickerSegue", sender: self)
        }
    }
}



