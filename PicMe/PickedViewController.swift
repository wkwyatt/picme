//
//  PickedViewController.swift
//  PicMe
//
//  Created by William Wyatt on 9/9/16.
//  Copyright © 2016 William Wyatt. All rights reserved.
//

import UIKit

class PickedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickedSegmentedControl: UISegmentedControl!
    
    var pickedImages:[String] = [String]()
    var startingImageZoom:CGRect?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.loadPickedImages("picked")
        self.pickedSegmentedControl.selectedSegmentIndex = 0
        // Do any additional setup after loading the view.
    }

    @IBAction func pickedSegmentedControlTouched(sender: AnyObject) {
        switch self.pickedSegmentedControl.selectedSegmentIndex {
        case 0:
            loadPickedImages("picked")
            break
        case 1:
            loadPickedImages("ditched")
            break
        default:
            break
        }
    }
    
    func loadPickedImages(list: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        self.pickedImages = defaults.objectForKey(list) as? [String] ?? [String]()
        self.tableView.reloadData()
    }
}

/**********  Table View Delegate Funcs  ************/

extension PickedViewController {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickedImages.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .Default, reuseIdentifier: "plainCell")
        let url = NSURL(string: pickedImages[indexPath.row])
        cell.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        cell.imageView?.loadImageUsingCacheWithURLString(url!)
        
        if cell.imageView?.image == nil {
            cell.imageView?.image = UIImage(named: "noButton")
        }
        
        cell.textLabel?.text = "\(indexPath.row + 1)"
        
        cell.imageView?.userInteractionEnabled = true
        cell.imageView?.tag = indexPath.row
        let tapped:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:
        #selector(tableViewCellImageTouched))
        tapped.numberOfTapsRequired = 1
        cell.imageView?.addGestureRecognizer(tapped)
        return cell
    }
    
    func tableViewCellImageTouched(sender: UITapGestureRecognizer) {
        let gesture:UITapGestureRecognizer = sender
        guard let imageRow = gesture.view?.tag else { return }
        guard let zoomFrame = gesture.view?.frame else { return }
        guard let url = NSURL(string: pickedImages[imageRow]) else { return }
        let indexPath = NSIndexPath(forRow: imageRow, inSection: 0)
        
        let imageView = UIImageView()
        imageView.loadImageUsingCacheWithURLString(url)
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        
        startingImageZoom = gesture.view?.superview?.convertRect(zoomFrame, toView: self.view)
        imageView.frame = startingImageZoom ?? zoomFrame
        
        
        self.zoomInTableViewCellImage(imageView, indexPath: indexPath)
    }
    
    func zoomInTableViewCellImage(imageSelected: UIImageView, indexPath: NSIndexPath) {
        imageSelected.userInteractionEnabled = true
        imageSelected.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(zoomOutTableViewCellImage)))
        self.view.addSubview(imageSelected)
        UIView.animateWithDuration(0.65) {
            imageSelected.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        }
    }
    
    func zoomOutTableViewCellImage(sender: UITapGestureRecognizer) {
        guard let zoomFrame = sender.view?.frame else { return }
        guard let startingImageFrame = sender.view?.superview?.convertRect(zoomFrame, toView: nil) else { return }
        
        UIView.animateWithDuration(0.65, animations: { 
            sender.view?.frame = self.startingImageZoom ?? startingImageFrame
            }) { (didComplete) in
                sender.view?.removeFromSuperview()
        }
    }
}